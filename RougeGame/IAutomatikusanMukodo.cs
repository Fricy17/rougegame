﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Automatizmus
{
    interface IAutomatikusanMukodo
    {
        int MukodesIntervallum { get; } //tizedmásodperc, az interface-t megvalósító objektum
        // Mukodik metodusa ennyi időközönként fog meghivódni.

        void Mukodik();
    }
}
