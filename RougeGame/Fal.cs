﻿using RougeGame.Jatek.Jatekter;
using RougeGame.Jatek.Megjelenites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Szabalyok
{
    class Fal : RogzitettJatekElem, IKirajzolhato
    {
        public Fal(int x, int y, JatekTer ter) : base(x,y,ter){}

        public override double Meret { get => 1; }

        public char Alak { get => '\u2593'; }

        public override void Utkozes(JatekElem elem){}
    }
}
