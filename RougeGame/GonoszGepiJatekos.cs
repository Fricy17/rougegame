﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RougeGame.Jatek.Jatekter;

namespace RougeGame.Jatek.Szabalyok
{
    class GonoszGepiJatekos : GepiJatekos
    {
        public GonoszGepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter){}

        public override char Alak { get => '\u2642'; }

        public override void Utkozes(JatekElem elem)
        {
            base.Utkozes(elem);
            if (Aktiv)
            {
                if (elem is Jatekos)
                    (elem as Jatekos).Serul(10);
                if (elem is GepiJatekos)
                    (elem as GepiJatekos).Serul(10);
            }
        }
    }
}
