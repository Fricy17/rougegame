﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Megjelenites
{
    public interface IKirajzolhato
    {
        int X { get; }
        int Y { get; }
        char Alak { get; }

    }
}
