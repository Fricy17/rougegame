﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RougeGame.Jatek.Jatekter;

namespace RougeGame.Jatek.Szabalyok
{
    class MozgasHelyHianyMiattNemSikerultKivetel : MozgasNemSikerultKivetel
    {
        JatekElem[] elemek;

        public MozgasHelyHianyMiattNemSikerultKivetel(JatekElem jatekElem, int x, int y, JatekElem[] elemek) : base(jatekElem, x, y){
            this.elemek = elemek;
        }

        public JatekElem[] Elemek { get => elemek; }
    }
}
