﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Megjelenites
{
    public interface IMegjelenitheto
    {
        int[] MegjelenitendoMeret { get; }

        IKirajzolhato[] MegjelenitendoElemek();
    }
}
