﻿using RougeGame.Jatek.Megjelenites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Jatekter
{
    class JatekTer: IMegjelenitheto
    {
        List<JatekElem> elemek;
        int meretX; //Játéktér maximális mérete x irányban.
        int meretY; //Játéktér maximális mérete y irányban.

        public int MeretX { get => meretX; }
        public int MeretY { get => meretY; }

        public int[] MegjelenitendoMeret { get => new int[2] { meretX, MeretY }; }

        public JatekTer(int meretX, int meretY)
        {
            elemek = new List<JatekElem>();
            this.meretX = meretX;
            this.meretY = meretY;
        }

        public void Felvetel(JatekElem elem)
        {
            elemek.Add(elem);   
        }

        public void Torles(JatekElem elem)
        {
            if (elemek.Contains(elem))
            {
                elemek.Remove(elem);
            }
   
        }

        public List<JatekElem> MegadottHelyenLevok(int x, int y, int tavolsag)
        {
            List<JatekElem> result = new List<JatekElem>();
            foreach (var item in elemek)
            {
                if(Math.Sqrt(Math.Pow(x-item.X,2) + Math.Pow(y - item.Y, 2))<= tavolsag)
                {
                    result.Add(item);
                }
            }
            return result;
        }

        public List<JatekElem> MegadottHelyenLevok(int x, int y)
        {
            return MegadottHelyenLevok(x, y, 0);
        }

        public IKirajzolhato[] MegjelenitendoElemek()
        {
            int counter = 0;
            for (int i = 0; i < elemek.Count; i++)
                if (elemek[i] is IKirajzolhato)
                    counter++;

            IKirajzolhato[] vissza = new IKirajzolhato[counter];

            counter = 0;
            for (int i = 0; i < elemek.Count; i++)
                if (elemek[i] is IKirajzolhato)
                {
                    vissza[counter] = elemek[i] as IKirajzolhato;
                    counter++;
                }

            return vissza;
        }
    }
}
