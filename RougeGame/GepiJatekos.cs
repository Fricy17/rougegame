﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RougeGame.Jatek.Jatekter;
using RougeGame.Jatek.Automatizmus;

namespace RougeGame.Jatek.Szabalyok
{
    class GepiJatekos : Jatekos, IAutomatikusanMukodo
    {
        static Random rnd = new Random();
        public GepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter) {}

        public override char Alak { get => '\u2640'; }

        public int MukodesIntervallum { get => 2; }

        
        public void Mozgas()
        {
            int irany = rnd.Next(0,4);
            bool hiba = false;
            int probalkozas = 0;
            do
            {
                hiba = false;
                try
                {
                    if (irany == 0)//Fel
                        Megy(0, 1);
                    if (irany == 1)//Jobbra
                        Megy(1, 0);
                    if (irany == 2) //Le
                        Megy(0, -1);
                    if (irany == 3) //Balra
                        Megy(-1, 0);
                }
                catch (MozgasHelyHianyMiattNemSikerultKivetel)
                {
                    hiba = true;
                    irany = (irany + 1) % 4;
                    probalkozas++;
                }
            } while (hiba == true || probalkozas==3); // ameddig! ez nem teljesül
        }

        public void Mukodik()
        {
            Mozgas();
        }
    }
}
