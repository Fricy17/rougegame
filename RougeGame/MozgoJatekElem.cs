﻿using RougeGame.Jatek.Jatekter;
using RougeGame.Jatek.Szabalyok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Jatekter
{
    abstract class MozgoJatekElem : JatekElem
    {
        bool aktiv = true;

        public MozgoJatekElem(int x, int y, JatekTer ter) : base(x,y,ter){}

        public bool Aktiv { get => aktiv; set => aktiv = value; }

        public void Athelyez(int ujx, int ujy)
        {
            double osszMeret = 0;
            foreach (var item in ter.MegadottHelyenLevok(ujx, ujy))
            {
                item.Utkozes(this);
                Utkozes(item);
                if (!aktiv) 
                {
                    throw new MozgasHalalMiattNemSikerultKivetel(this, ujx, ujy);
                }
                
            }
            if (aktiv)
            {
                foreach (var item in ter.MegadottHelyenLevok(ujx, ujy))
                {
                    osszMeret += item.Meret;
                }
            }
            if (osszMeret + this.Meret < 1)
            {
                this.X = ujx;
                this.Y = ujy;
            }
            else {
                List<JatekElem> temp = ter.MegadottHelyenLevok(ujx, ujy);
                JatekElem[] elemek = new JatekElem[temp.Count];
                for(int i = 0; i<elemek.Length; i++)
                {
                    elemek[i] = temp[i];
                }
                throw new MozgasHelyHianyMiattNemSikerultKivetel(this, ujx, ujy, elemek);
            }
           
        }
        
    }
}
