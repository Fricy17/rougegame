﻿using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Szabalyok
{
    class MozgasNemSikerultKivetel : Exception
    {
        JatekElem akiNemTudottLepni;
        int x;
        int y;

        public int X { get => x;}
        public int Y { get => y;}
        internal JatekElem AkiNemTudottLepni { get => akiNemTudottLepni; }

        public MozgasNemSikerultKivetel(JatekElem akiNemTudottLepni, int x, int y)
        {
            this.akiNemTudottLepni = akiNemTudottLepni;
            this.x = x;
            this.y = y;
        }
    }
}
