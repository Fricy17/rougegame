﻿using RogueGame.Jatek.Megjelenites;
using RougeGame.Jatek.Automatizmus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Megjelenites
{
    class KonzolosMegjelenito : IAutomatikusanMukodo
    {
        IMegjelenitheto forras; // innen jönnek majd a megjelenitendő adatok

        int pozX;  // ennyivel kell majd eltolni a kirajzolandó adatok x koordinátáját
        int pozY;
        //(ez akkor lesz érdekes ha több konzolos megjelenitő is dolgozik egyszerre egy időben, akkor
        //más - más helyre rajzolják ki az adataikat)

        public KonzolosMegjelenito(IMegjelenitheto forras, int pozX, int pozY)
        {
            this.forras = forras;
            this.pozX = pozX;
            this.pozY = pozY;
        }

        public int MukodesIntervallum { get => 1; }

        public void Megjelenites()
        {
            IKirajzolhato[] elemek = forras.MegjelenitendoElemek();
            int[] meret = forras.MegjelenitendoMeret;
            for(int i = 0; i < meret[1]; i++)
            {
                for (int j = 0; j < meret[0]; j++)
                {
                    char c = ' ';
                    for(int k = 0; k < elemek.Length; k++)
                    {
                        if (elemek[k].X == i && elemek[k].Y == j)
                            c = elemek[k].Alak;
                    }

                    SzalbiztosKonzol.KiirasXY(i + pozX, j + pozY, c);
                }
            }
        }

        public void Mukodik()
        {
            Megjelenites();
        }
    }
}
