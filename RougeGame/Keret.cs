﻿using RougeGame.Jatek.Automatizmus;
using RougeGame.Jatek.Jatekter;
using RougeGame.Jatek.Megjelenites;
using RougeGame.Jatek.Szabalyok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Keret
{
    class Keret
    {
        static Random rnd = new Random();
        const int PALYA_MERET_X = 20;
        const int PALYA_MERET_Y = 20; 
        const int KINCSEK_SZÁMA = 10;
        int megtalaltKincsek = 0;
        JatekTer ter;
        bool jatekVege = false;
        OrajelGenerator generator;

        public Keret()
        {
            ter = new JatekTer(PALYA_MERET_X, PALYA_MERET_Y);
            PalyaGeneralas();
            generator = new OrajelGenerator();
        }

        void PalyaGeneralas()
        {
            int falN = 0;
            Fal[] falak = new Fal[PALYA_MERET_X * PALYA_MERET_Y];
            falak[0] = new Fal(2, 2, ter);
            falN = 1;

            for (int i=0; i<PALYA_MERET_X; i++)
            {
                new Fal(0, i, ter);
                new Fal(PALYA_MERET_Y - 1, i, ter);
            }
            for (int i = 1; i < PALYA_MERET_Y-1; i++) 
            {
                new Fal(i,0,ter);
                new Fal(i,PALYA_MERET_X - 1, ter);
            }

            int kivalasztottIndex = rnd.Next(0, falN - 1);
            int kivalasztottIrany = rnd.Next(0, 4);
            Fal kivalasztottFal = falak[kivalasztottIndex];
            bool folytat = false;

            int probalkozas = 0;

            do
            {
                if (probalkozas == 0) {
                    kivalasztottIndex = rnd.Next(0, falN - 1);
                    kivalasztottIrany = rnd.Next(0, 4);
                    kivalasztottFal = falak[kivalasztottIndex];
                }
                if (probalkozas == 4)
                {
                    kivalasztottIndex += 1;
                    if (falak[kivalasztottIndex] != null)
                    {
                        kivalasztottFal = falak[kivalasztottIndex];
                        probalkozas = 0;
                    }
                    else
                    {
                        folytat = true;
                        continue;
                    }
                }

                switch (kivalasztottIrany)
                {
                    case 0: //Balra
                        
                        if ( kivalasztottFal.Y - 2 > 0 &&
                            ter.MegadottHelyenLevok(kivalasztottFal.X, kivalasztottFal.Y - 2).Count == 0)
                        {
                            falak[falN] = new Fal(kivalasztottFal.X, kivalasztottFal.Y - 2, ter);
                            new Fal(kivalasztottFal.X, kivalasztottFal.Y - 1, ter);
                            falN++;
                            probalkozas = 0;
                            continue;
                        }
                        kivalasztottIrany = ++kivalasztottIrany % 4;
                        probalkozas++;
                        break;
                        
                    case 1: // Jobbra
                        
                        if (kivalasztottFal.Y + 2 < PALYA_MERET_X &&
                            ter.MegadottHelyenLevok(kivalasztottFal.X, kivalasztottFal.Y + 2).Count == 0)
                        {
                            falak[falN] = new Fal(kivalasztottFal.X, kivalasztottFal.Y + 2, ter);
                            new Fal(kivalasztottFal.X, kivalasztottFal.Y + 1, ter);
                            falN++;
                            probalkozas = 0;
                            continue;
                        }
                        kivalasztottIrany = ++kivalasztottIrany % 4;
                        probalkozas++;
                        break;

                    case 2: // Le
                        
                        if (kivalasztottFal.X + 2 < PALYA_MERET_Y &&
                            ter.MegadottHelyenLevok(kivalasztottFal.X + 2, kivalasztottFal.Y).Count == 0)
                        {
                            falak[falN] = new Fal(kivalasztottFal.X + 2, kivalasztottFal.Y, ter);
                            new Fal(kivalasztottFal.X + 1, kivalasztottFal.Y, ter);
                            falN++;
                            probalkozas = 0;
                            continue;
                        }
                        kivalasztottIrany = ++kivalasztottIrany % 4;
                        probalkozas++;
                        break;
                        
                    case 3: // Fel
                        
                        if (kivalasztottFal.X - 2 > 0 && ter.MegadottHelyenLevok(kivalasztottFal.X - 2, kivalasztottFal.Y).Count == 0)
                        {
                            falak[falN] = new Fal(kivalasztottFal.X - 2, kivalasztottFal.Y, ter);
                            new Fal(kivalasztottFal.X - 1, kivalasztottFal.Y, ter);
                            falN++;
                            probalkozas = 0;
                            continue;
                        }
                        kivalasztottIrany = ++kivalasztottIrany % 4;
                        probalkozas++;  
                        break;
                }
            } while (!folytat);

            //KINCSEK
            
            for (int i = 0; i < KINCSEK_SZÁMA; i++)
            {
                
                int x = rnd.Next(1, PALYA_MERET_Y);
                int y = rnd.Next(1, PALYA_MERET_X);

                if (x != 1 && y != 1 &&
                    x != PALYA_MERET_Y - 2 && y != PALYA_MERET_X - 2 &&
                    ter.MegadottHelyenLevok(x, y, 2).All(elem => (elem as Kincs)?.Alak != '\u2666' ) &&
                    ter.MegadottHelyenLevok(x,y).Count == 0
                    )
                {
                    Kincs temp = new Kincs(x, y, ter);
                    temp.KincsFelvetel += KincsFelvetelTortent;
                }
                else
                {
                    i--;
                }
                    
            }
        }

        private int [] Jatekoskoordinatak() {
            bool folytat = false;
            int[] result = new int[2];
            int oldal = rnd.Next(0, 4);
            do
            {
                switch (oldal)
                {
                    case 0: // fenti
                        result[0] = 1;
                        result[1] = rnd.Next(1, PALYA_MERET_Y - 2);
                        break;
                    case 1: // lenti
                        result[0] = PALYA_MERET_Y - 2;
                        result[1] = rnd.Next(1, PALYA_MERET_Y - 2);
                        break;
                    case 2: // jobb oldal
                        result[0] = rnd.Next(1, PALYA_MERET_X - 2);
                        result[1] = 1;
                        break;
                    case 3: // bal oldal
                        result[0] = rnd.Next(1, PALYA_MERET_X - 2);
                        result[1] = PALYA_MERET_X - 2;
                        break;
                }
                if (ter.MegadottHelyenLevok(result[0], result[1], 5).All(elem => (elem as Jatekos)?.Alak != '\u263B') &&
                    ter.MegadottHelyenLevok(result[0], result[1], 2).All(elem => (elem as Kincs)?.Alak != '\u2666')
                    )
                {
                    folytat = true;
                }
            } while (!folytat);
            //ter.MegadottHelyenLevok(result[0], result[1], 5).Any(elem => (elem as Jatekos)?.Alak == '\u263B')
            return result;
        }
        public void Futtatas()
        {
            KonzolosEredmenyAblak eredmenyAblak = new KonzolosEredmenyAblak(0,22,5);
            
            Jatekos jatekos = new Jatekos("Bela", 1, 1, ter);
            jatekos.JatekosValtozas += JatekosValtozasTortent;
            eredmenyAblak.JatekosFeliratkozas(jatekos);

            int[] koord = Jatekoskoordinatak();
            //Random oldalt kezd
            GepiJatekos gepiJatekos = new GepiJatekos("Kati", koord[0], koord[1], ter);

            koord = Jatekoskoordinatak();
            GonoszGepiJatekos gonoszGepiJatekos = new GonoszGepiJatekos("Laci", koord[0], koord[1], ter);
            generator.Felvetel(gepiJatekos);
            generator.Felvetel(gonoszGepiJatekos);
            
            KonzolosMegjelenito km = new KonzolosMegjelenito(ter,0,0);
            KonzolosMegjelenito km2 = new KonzolosMegjelenito(jatekos, 25, 0);
            KonzolosMegjelenito km3 = new KonzolosMegjelenito(gepiJatekos, 50, 0);
            //KonzolosMegjelenito km4 = new KonzolosMegjelenito(gonoszGepiJatekos, 75,0);

            generator.Felvetel(km);
            generator.Felvetel(km2);
            generator.Felvetel(km3);
            //generator.Felvetel(km4);

            while (!jatekVege)
            {
                try
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    if (key.Key == ConsoleKey.LeftArrow) jatekos.Megy(-1, 0);
                    if (key.Key == ConsoleKey.RightArrow) jatekos.Megy(1, 0);
                    if (key.Key == ConsoleKey.UpArrow) jatekos.Megy(0, -1);
                    if (key.Key == ConsoleKey.DownArrow) jatekos.Megy(0, 1);
                    if (key.Key == ConsoleKey.Escape) jatekVege = true;
                }
                catch (MozgasHelyHianyMiattNemSikerultKivetel e)
                {
                    System.Console.Beep(500 + e.Elemek.Length * 100, 10);
                }
            }
        }

        void KincsFelvetelTortent(Kincs k, Jatekos j)
        {
            megtalaltKincsek++;
            if (megtalaltKincsek == KINCSEK_SZÁMA)
                jatekVege = true;
        }

        void JatekosValtozasTortent(Jatekos j, int ujPontszam, int ujEletero)
        {
            if (ujEletero == 0)
                jatekVege = true;
        }

    }
}
