﻿using RogueGame.Jatek.Megjelenites;
using RougeGame.Jatek.Szabalyok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Megjelenites
{
    class KonzolosEredmenyAblak
    {
        int pozX;
        int pozY;
        int maxSorSzam; 
        int sor = 0; 

        public KonzolosEredmenyAblak(int pozX, int pozY, int maxSorSzam)
        {
            this.pozX = pozX;
            this.pozY = pozY;
            this.maxSorSzam = maxSorSzam;
        }

        public void JatekosValtozasTortent(Jatekos j, int ujPontszam, int ujEletero)
        {
            string s = "Játékos neve: " + j.Nev + ", pontszáma: " + ujPontszam + ", életereje: " + ujEletero;
            SzalbiztosKonzol.KiirasXY(pozX, pozY, s);
            if (sor > maxSorSzam)
                sor = 0;
            else
                sor++;
        }

        public void JatekosFeliratkozas(Jatekos j) {
            // j JatékosVáltozas esemenyere iratkozzunk fel az előző metódussal
            j.JatekosValtozas += JatekosValtozasTortent;
        }
    }
}
