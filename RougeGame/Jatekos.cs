﻿using RogueGame.Jatek.Megjelenites;
using RougeGame.Jatek.Jatekter;
using RougeGame.Jatek.Megjelenites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace RougeGame.Jatek.Szabalyok
{
    delegate void KincsFelvetelKezelo(Kincs k, Jatekos j);
    delegate void JatekosValtozasKezelo(Jatekos j, int ujPontszam, int ujEletero);

    class Jatekos : MozgoJatekElem, IKirajzolhato, IMegjelenitheto
    {
        string nev;
        int eletero = 100;
        int pontszam = 0;

        public event JatekosValtozasKezelo JatekosValtozas; //eseménykezelő

        public Jatekos(string nev, int x, int y, JatekTer ter) : base (x,y,ter)
        {
            this.nev = nev;
        }

        public string Nev { get => nev;}

        public override double Meret { get => 0.2; }

        public int Pontszam { get => pontszam;}

        public virtual char Alak { get => Aktiv == false? '\u263A' : '\u263B' ; }

        public int[] MegjelenitendoMeret { get => new int[2] { ter.MeretX, ter.MeretY }; }

        public void Serul(int sebzes)
        {
            if (eletero > 0)
            {
                eletero -= sebzes;
                if (eletero < 0)
                {
                    eletero = 0;
                    Aktiv = false;
                }
                if (JatekosValtozas != null)
                    JatekosValtozas(this, pontszam, eletero);
            }

        }

        public void PontotSzerez(int pont)
        {
            pontszam += pont;
            if (JatekosValtozas != null)
                JatekosValtozas(this, pontszam, eletero);
        }

        public void Megy(int rx, int ry) // -1, 0, 1 értékeket várunk
        {
            Athelyez(X + rx, Y + ry);            
        }

        public override void Utkozes(JatekElem elem){}

        public IKirajzolhato[] MegjelenitendoElemek()
        {
            List<JatekElem> jElemek = ter.MegadottHelyenLevok(this.X, this.Y, 5);
            int counter = 0;

            for (int i = 0; i < jElemek.Count; i++)
                if (jElemek[i] is IKirajzolhato)
                    counter++;

            IKirajzolhato[] vissza = new IKirajzolhato[counter];

            counter = 0;
            for (int i = 0; i < jElemek.Count; i++)
                if (jElemek[i] is IKirajzolhato)
                {
                    vissza[counter] = jElemek[i] as IKirajzolhato;
                    counter++;
                }
            return vissza;
        }
    }
}
