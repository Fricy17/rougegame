﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Jatekter
{
    abstract class JatekElem
    {
        int x;
        int y;
        protected JatekTer ter;
        
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        abstract public double Meret { get;} //0-1 közötti érték

        public JatekElem(int x, int y, JatekTer ter)
        {
            this.x = x;
            this.y = y;
            this.ter = ter;
            ter.Felvetel(this);
        }

        abstract public void Utkozes(JatekElem elem);
    }
}
