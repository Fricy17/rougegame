﻿using RougeGame.Jatek.Jatekter;
using RougeGame.Jatek.Megjelenites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace RougeGame.Jatek.Szabalyok
{
    class Kincs : RogzitettJatekElem, IKirajzolhato
    {
        public event KincsFelvetelKezelo KincsFelvetel;

        public Kincs(int x, int y, JatekTer ter) : base(x,y,ter){ }

        public override double Meret { get => 1; }

        public char Alak { get => '\u2666'; }

        public override void Utkozes(JatekElem elem)
        {
            if (elem is Jatekos)
            {
                ter.Torles(this);
                (elem as Jatekos).PontotSzerez(50);
                if (KincsFelvetel != null)
                    KincsFelvetel(this, elem as Jatekos);
            }
            
        }

    }
}
